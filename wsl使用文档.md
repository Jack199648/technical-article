# wsl使用文档

## wsl常用命令

```shell
wsl --help # 查看wsl帮助
wsl -l -v # 查看当前安装的发行版
wsl -l -o # 查看可供安装的发行版
wsl --install -d Ubuntu-20.04 # 安装指定的发行版
wsl --export Ubuntu-20.04 D:\wsl\images\Ubuntu-20.04.tar # 导出ubuntu-20.04到指定的路径
wsl --unregister Ubuntu-20.04 # 注销并清楚指定的发行版
wsl --import Ubuntu-20.04 d:\wsl\Ubuntu-20.04 D:\wsl\images\Ubuntu-20.04.tar # 导入ubuntu-20.04到d:\wsl\Ubuntu-20.04目录下
```

## wsl安装docker

```shell
apt update && apt upgrade -y
curl -fsSL https://get.docker.com -o get-docker.sh  # 下载docker安装脚本,不同于完全linux虚拟机方式，WLS2下通过apt install docker-ce命令安装的docker无法启动，因为WSL2方式的ubuntu里面没有systemd。上述官方get-docker.sh安装的docker，dockerd进程是用ubuntu传统的init方式而非systemd启动的
sh get-docker.sh  # 执行脚本安装过程中，脚本提示“建议使用Docker Desktop for windows”，20s内按Ctrl+C会退出安装，所以需要等待20s，另外此种方式需要访问外网
service docker start
service docker status
```

