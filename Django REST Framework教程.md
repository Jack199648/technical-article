# Django REST Framework教程

[中文教程参考](https://q1mi.github.io/Django-REST-framework-documentation/)



## 基础介绍

```shell
# 请求方法
[POST]    https://api.example.com/v1/users   // 新增
[GET]     https://api.example.com/v1/users/1 // 查询
[PATCH]   https://api.example.com/v1/users/1 // 更新
[PUT]     https://api.example.com/v1/users/1 // 覆盖，全部更新
[DELETE]  https://api.example.com/v1/users/1 // 删除

GET（SELECT）：从服务器取出资源（一项或多项）。
POST（CREATE）：在服务器新建一个资源。
PUT（UPDATE）：在服务器更新资源（客户端提供改变后的完整资源）。
PATCH（UPDATE）：在服务器更新资源（客户端提供改变的属性）。
DELETE（DELETE）：从服务器删除资源。


# 过滤信息
?limit=10：指定返回记录的数量
?offset=10：指定返回记录的开始位置。
?page=2&per_page=100：指定第几页，以及每页的记录数。
?sortby=name&order=asc：指定返回结果按照哪个姓名排序，以及排序顺序。
?user_type_id=1：指定筛选条件，比如用户类型
DRF与django-filter联用可以轻松实现过滤


# 状态码
200 OK - [GET]：服务器成功返回用户请求的数据，该操作是幂等的（Idempotent）。201 CREATED - [POST/PUT/PATCH]：用户新建或修改数据成功。
202 Accepted - [*]：表示一个请求已经进入后台排队（异步任务）
204 NO CONTENT - [DELETE]：用户删除数据成功。
400 INVALID REQUEST - [POST/PUT/PATCH]：用户发出的请求有错误，服务器没有进行新建或修改数据的操作，该操作是幂等的。
401 Unauthorized - [*]：表示用户没有权限（令牌、用户名、密码错误）。
403 Forbidden - [*] 表示用户得到授权（与401错误相对），但是访问是被禁止的。
404 NOT FOUND - [*]：用户发出的请求针对的是不存在的记录，服务器没有进行操作，该操作是幂等的。
406 Not Acceptable - [GET]：用户请求的格式不可得（比如用户请求JSON格式，但是只有XML格式）。
410 Gone -[GET]：用户请求的资源被永久删除，且不会再得到的。
422 Unprocesable entity - [POST/PUT/PATCH] 当创建一个对象时，发生一个验证错误。
500 INTERNAL SERVER ERROR - [*]：服务器发生错误
DRF给出Respone时可以指定各种各样状态码，很容易使用
```



```shell
pip install django
pip install djangorestframework
django-admin.py startproject apiproject # 创建项目
cd apiproject # 进入项目目录
python manage.py startapp blog # 创建应用

# 在settings里注册应用
INSTALLED_APPS =(
    ...
    'rest_framework',
    'blog',
)

python manage.py makemigrations # 在添加了models后,生成数据库迁移文件
python manage.py migrate  # 执行数据库迁移文件

python manage.py createsuperuser # 创建管理员账户
```



##  **自定义序列化器(serializers.py)** 

 利用DRF开发Web API的第一步总是自定义序列化器(serializers) 。序列化器的作用是将模型实例(比如用户、文章)序列化和反序列化为诸如`json`之类的表示形式。一个模型实例可能有许多字段属性，但一般情况下你不需要把所有字段信息以JSON格式数据返回给用户。序列化器定义了需要对一个模型实例的哪些字段进行序列化/反序列化, 并可对客户端发送过来的数据进行验证和存储。 

 就像Django提供了`Form`类和`ModelForm`类两种方式自定义表单一样，REST framework提供了`Serializer`类和`ModelSerializer`类两种方式供你自定义序列化器。前者需手动指定需要序列化和反序列化的字段，后者根据模型(model)生成需要序列化和反序列化的字段，可以使代码更简洁。 



### 使用Serializers类

```python
class ArticleSerializer(serializers.Serializer):
  	# 序列化器类的第一部分定义了序列化/反序列化的字段
    # 注意：定义序列化器时一定要注明哪些是仅可读字段(read-only fields)，哪些是普通字段。对于read-only fields，客户端是不需要也不能够通过POST或PUT请求提交相关数据进行反序列化的。
    id = serializers.IntegerField(read_only=True)
    title = serializers.CharField(required=True, allow_blank=True, max_length=90)
    body = serializers.CharField(required=False, allow_blank=True)
    author = serializers.ReadOnlyField(source="author.id")
    status = serializers.ChoiceField(choices=Article.STATUS_CHOICES, default='p')
    create_date = serializers.DateTimeField(read_only=True)

    # create()和update()方法定义了在调用serializer.save()时如何创建和修改完整的实例。
    
    def create(self, validated_data):
        """
        Create a new "article" instance
        """
        return Article.objects.create(**validated_data)

    def update(self, instance, validated_data):
        """
        Use validated data to return an existing `Article`instance。"""
        instance.title = validated_data.get('title', instance.title)
        instance.body = validated_data.get('body', instance.body)
        instance.status = validated_data.get('status', instance.status)
        instance.save()
        return instance
```

### 使用ModelSerializers类

```python
# 我们的ArticleSerializer类中重复了很多包含在Article模型（model）中的字段信息。使用ModelSerializer类可以重构我们的序列化器类，使整体代码更简洁
class ArticleSerializer(serializers.ModelSerializer):

    class Meta:
        model = Article
        fields = '__all__'
        read_only_fields = ('id', 'author', 'create_date')
```





##  **编写API视图(views.py)** 

### 使用基于函数的试图(FBV)

#### ` @api_view `装饰器

- 与Django传统函数视图相区分，强调这是API视图，并限定了可以接受的请求方法。
- 拓展了django原来的request对象。新的request对象不仅仅支持request.POST提交的数据，还支持其它请求方式如PUT或PATCH等方式提交的数据，所有的数据都在`request.data`字典里。这对开发Web API非常有用

```
request.POST  # 只处理表单数据  只适用于'POST'方法
request.data  # 处理任意数据  适用于'POST'，'PUT'和'PATCH'方法
```

```python
@api_view(['GET', 'POST'])
def article_list(request):

    """
    List all articles, or create a new article.
    """
    if request.method == 'GET':
        articles = Article.objects.all()
        serializer = ArticleSerializer(articles, many=True)
        return Response(serializer.data)

    elif request.method == 'POST':
        serializer = ArticleSerializer(data=request.data)
        if serializer.is_valid():
            # 由于序列化器中author是read-only字段，用户是无法通过POST提交来修改的，我们在创建Article实例时需手动将author和request.user绑定
            serializer.save(author=request.user)
            return Response(serializer.data, status=status.HTTP_201_CREATED)
        # 不再显式地将请求或响应绑定到特定的内容类型比如HttpResponse和JSONResponse，我们统一使用Response方法返回响应，该方法支持内容协商，可根据客户端请求的内容类型返回不同的响应数据。
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)
```



###  **使用基于类的视图(CBV)** 

 一个中大型的Web项目代码量通常是非常大的，如果全部使用函数视图写，那么代码的复用率是非常低的。而使用类视图呢，就可以有效的提高代码复用，因为类是可以被继承的，可以拓展的。特别是将一些可以共用的功能抽象成Mixin类或基类后可以减少重复造轮子的工作。 

DRF推荐使用基于类的视图(CBV)来开发API, 并提供了4种开发CBV开发模式。

- 使用基础APIView类
- 使用Mixins类和GenericAPI类混配
- 使用通用视图generics.*类, 比如 generics.ListCreateAPIView
- 使用视图集ViewSet和ModelViewSet

####  **使用基础APIView类** 

 	DRF的APIView类继承了Django自带的View类, 一样可以按请求方法调用不同的处理函数，比如get方法处理GET请求，post方法处理POST请求。**不过DRF的APIView要强大得多。它不仅支持更多请求方法，而且对Django的request对象进行了封装，可以使用request.data获取用户通过POST, PUT和PATCH方法发过来的数据，而且支持插拔式地配置认证、权限和限流类。** 

####  **使用Mixin类和GenericAPI类混配** 

 	GenericAPIView 类继承了APIView类，提供了基础的API视图。它对用户请求进行了转发，并对Django自带的request对象进行了封装。不过它比APIView类更强大，**因为它还可以通过queryset和serializer_class属性指定需要序列化与反序列化的模型或queryset及所用到的序列化器类。** 

​	 .perform_create这个钩子函数是CreateModelMixin类自带的，用于执行创建对象时需要执行的其它方法，比如发送邮件等功能，有点类似于Django的信号。类似的钩子函数还有UpdateModelMixin提供的.perform_update方法和DestroyModelMixin提供的.perform_destroy方法。 	

####  **使用通用视图generics.\*类** 

​	 `generics.ListCreateAPIView`类支持List、Create两种视图功能，分别对应GET和POST请求。`generics.RetrieveUpdateDestroyAPIView`支持Retrieve、Update、Destroy操作，其对应方法分别是GET、PUT和DELETE 

 	其它常用generics.*类视图还包括`ListAPIView`, `RetrieveAPIView`, `RetrieveUpdateAPIView`等等 

```python

# 通用视图
from rest_framework import generics

class ArticleList(generics.ListCreateAPIView):
    queryset = Article.objects.all()
    serializer_class = ArticleSerializer

    # 将request.user与author绑定
    def perform_create(self, serializer):
        serializer.save(author=self.request.user)

class ArticleDetail(generics.RetrieveUpdateDestroyAPIView):
    queryset = Article.objects.all()
    serializer_class =ArticleSerializer
    
```



####  **使用视图集(viewset)** 

 	使用通用视图generics.*类后视图代码已经大大简化，但是ArticleList和ArticleDetail两个类中queryset和serializer_class属性依然存在代码重复。使用视图集可以将两个类视图进一步合并，一次性提供`List、Create、Retrieve、Update、Destroy`这5种常见操作，这样queryset和seralizer_class属性也只需定义一次就好 

```python
# blog/views.py
from rest_framework import viewsets

class ArticleViewSet(viewsets.ModelViewSet):
    # 用一个视图集替代ArticleList和ArticleDetail两个视图
    queryset = Article.objects.all()
    serializer_class = ArticleSerializer
    # 自行添加，将request.user与author绑定
    def perform_create(self, serializer):
        serializer.save(author=self.request.user)
        
# blog/urls.py

from django.urls import re_path
from rest_framework.urlpatterns import format_suffix_patterns
from . import views
from rest_framework.routers import DefaultRouter

router = DefaultRouter()
router.register(r'articles', viewset=views.ArticleViewSet)

urlpatterns = []

urlpatterns += router.urls



# 一个视图集对应List、Create、Retrieve、Update、Destroy这5种操作,如果只需要其中几种方法,可以指定方法的映射
article_list = views.ArticleViewSet.as_view(
    {
        'get': 'list',
        'post': 'create'
    })

article_detail = views.ArticleViewSet.as_view({
    'get': 'retrieve', # 只处理get请求，获取单个记录
})

urlpatterns = [
    re_path(r'^articles/$', article_list),
    re_path(r'^articles/(?P<pk>[0-9]+)$', article_detail),
]

urlpatterns = format_suffix_patterns(urlpatterns)
```

####  使用哪种CBV类更好? 

 由于mixin类和GenericAPI的混用，这个和generics.*类没什么区别 ,所以直接看generics.*类即可。三种方式的对比:

- **基础的API类**：可读性最高，代码最多，灵活性最高。当你需要对的API行为进行个性化定制时，建议使用这种方式。
- **通用generics.*类**：可读性好，代码适中，灵活性较高。当你需要对一个模型进行标准的增删查改全部或部分操作时建议使用这种方式。
- **使用视图集viewset**: 可读性较低，代码最少，灵活性最低。当你需要对一个模型进行标准的增删查改的全部操作且不需定制API行为时建议使用这种方式。

## 玩转序列化器(Serializer)

### 修改序列化字段的数据格式

1.  **指定source来源** 
2.  **使用SerializerMethodField自定义方法** 
3.  **使用嵌套序列化器** 

```python
class UserSerializer(serializers.ModelSerializer):
    class Meta:
        model = User
        fields = ('id', 'username', 'email')


class ArticleSerializer(serializers.ModelSerializer):
    # 方法1:使用指定source来源新增两个可读的字段,用以覆盖原来Article模型默认的字段，其中指定author字段的来源(source)为原单个author对象的username，status字段为get_status_display方法返回的完整状态
    # author = serializers.ReadOnlyField(source="author.username")
    full_status = serializers.ReadOnlyField(source="get_status_display")

    # 方法2:使用SerializerMethodField自定义方法
    cn_status = serializers.SerializerMethodField()

    # 方法3:使用嵌套序列化器
    author = UserSerializer(read_only=True)  # 设置required=False表示可以接受匿名用户

    class Meta:
        model = Article
        fields = '__all__'
        read_only_fields = ('id', 'author', 'create_date')

    def get_cn_status(self, obj):
    		if obj.status == 'p':
            return '已发表'
    		elif obj.status == 'd':
            return '草稿'
        else:
            return ''
```

### 数据验证 (Validation)

 在反序列化数据时，在尝试访问经过验证的数据或保存对象实例之前，总是需要调用 `is_valid()`方法。如果发生任何验证错误，`.errors` 属性将包含表示结果错误消息的字典 

```python
serializer = CommentSerializer(data={'email': 'foobar', 'content': 'baz'})
serializer.is_valid()
# False
serializer.errors
# {'email': [u'Enter a valid e-mail address.'], 'created': [u'This field is required.']}
```

#### 引发无效数据的异常 (Raising an exception on invalid data)

 `.is_valid()` 方法使用可选的 `raise_exception` 标志，如果存在验证错误，将会抛出 `serializers.ValidationError` 异常 

 这些异常由 REST framework 提供的默认异常处理程序自动处理，默认情况下将返回 `HTTP 400 Bad Request` 响应 

```python
# Return a 400 response if the data was invalid.
serializer.is_valid(raise_exception=True)
```



#### 字段级别验证 (Field-level validation)

 `validate_<field_name>` 方法应该返回已验证的值或抛出 `serializers.ValidationError` 异常 

```python
class ArticleSerializer(serializers.Serializer):
    title = serializers.CharField(max_length=100)

    def validate_title(self, value):
        """
        Check that the article is about Django.
        """
        if 'django' not in value.lower():
            raise serializers.ValidationError("Article is not about Django")
        return value
```

 如果在您的序列化器上声明了 `  <field_name> ` 的参数为 `required=False`，那么如果不包含该字段，则此验证步骤不会发生 

#### 对象级别验证 (Object-level validation)

 `.validate()` 的方法  采用单个参数，该参数是字段值的字典。如果需要，它应该抛出 `ValidationError` 异常，或者只返回经过验证的值 

```python
class EventSerializer(serializers.Serializer):
    description = serializers.CharField(max_length=100)
    start = serializers.DateTimeField()
    finish = serializers.DateTimeField()

    def validate(self, data):
        """
        Check that the start is before the stop.
        """
        if data['start'] > data['finish']:
            raise serializers.ValidationError("finish must occur after start")
        return data
```



#### 验证器 (Validators)

 序列化器上的各个字段都可以包含验证器，通过在字段实例上声明 

```python

def multiple_of_ten(value):
    if value % 10 != 0:
        raise serializers.ValidationError('Not a multiple of ten')

class GameRecord(serializers.Serializer):
    score = IntegerField(validators=[multiple_of_ten])
    ...
```

 DRF还提供了很多可重用的验证器，比如**UniqueValidator**,**UniqueTogetherValidator**等等。通过在内部 `Meta` 类上声明来包含这些验证器 

```python

class EventSerializer(serializers.Serializer):
    name = serializers.CharField()
    room_number = serializers.IntegerField(choices=[101, 102, 103, 201])
    date = serializers.DateField()

    class Meta:
        # 房间号和日期的组合必须唯一.
        validators = UniqueTogetherValidator(
            queryset=Event.objects.all(),
            fields=['room_number', 'date']
        )  
```



###  重写序列化器的create和update方法 

 因为序列化器使用嵌套后，创建和更新的行为可能不明确，并且可能需要相关模型之间的复杂依赖关系，REST framework 3 要求你始终显式的编写这些方法。默认的 `ModelSerializer` `.create()` 和 `.update()` 方法不包括对可写嵌套表示的支持，所以我们总是需要对create和update方法进行重写。 

```python

class UserSerializer(serializers.ModelSerializer):
    profile = ProfileSerializer()

    class Meta:
        model = User
        fields = ('username', 'email', 'profile')

    def create(self, validated_data):
        profile_data = validated_data.pop('profile')
        user = User.objects.create(**validated_data)
        Profile.objects.create(user=user, **profile_data)
        return user
     
    
    def update(self, instance, validated_data):
            profile_data = validated_data.pop('profile')
            # 除非应用程序正确地强制始终设置该字段，否则就应该抛出一个需要处理的`DoesNotExist`。
            profile = instance.profile

            instance.username = validated_data.get('username', instance.username)
            instance.email = validated_data.get('email', instance.email)
            instance.save()

            profile.is_premium_member = profile_data.get(
                'is_premium_member',
                profile.is_premium_member
            )
            profile.has_support_contract = profile_data.get(
                'has_support_contract',
                profile.has_support_contract
             )
            profile.save()

            return instance
```

## 认证(Authentication)与权限(Permission)初识

 认证是通过用户提供的用户ID/密码组合或者Token来验证用户的身份。

权限(Permission)的校验发生验证用户身份以后，是由系统根据分配权限确定用户可以访问何种资源以及对这种资源进行何种操作，这个过程也被称为授权(Authorization) 

 在Django传统视图开发中你可能会使用`@login_required`和`@permission_required`这样的装饰器要求用户先登录或进行权限验证。在DRF中你不需要做，这是因为REST framework 包含许多默认权限类，我们可以用来限制谁可以访问给定的视图。在这种情况下，我们需要的是 `IsAuthenticatedOrReadOnly` 类，它将确保经过身份验证的请求获得读写访问权限，未经身份验证的请求将获得只读读的权限 

```python
# blog/views 给视图添加访问权限控制
class ArticleList(generics.ListCreateAPIView):
    queryset = Article.objects.all()
    serializer_class = ArticleSerializer
    permission_classes = (permissions.IsAuthenticatedOrReadOnly,)  # 给视图加权限

    # 将request.user与author绑定
    def perform_create(self, serializer):
        serializer.save(author=self.request.user)


class ArticleDetail(generics.RetrieveUpdateDestroyAPIView):
    queryset = Article.objects.all()
    serializer_class = ArticleSerializer
    permission_classes = (permissions.IsAuthenticatedOrReadOnly,)  # 给视图加权限

# myproject/urls
# 用户访问api-auth/login/就可以跳转到DRF的登陆界面
from django.contrib import admin
from django.urls import path, include

urlpatterns = [
    path('admin/', admin.site.urls),
    path('v1/', include('blog.urls')),
    path('api-auth/', include('rest_framework.urls')), # 用户登录页面
]
```

###  常用DRF自带权限类 

-  `IsAuthenticatedOrReadOnly` 类 :经过身份验证的请求获得读写访问权限，未经身份验证的请求将获得只读读的权限;
- `IsAuthenticated`类：仅限已经通过身份验证的用户访问；
- `AllowAny`类：允许任何用户访问；
- `IsAdminUser`类：仅限管理员访问；
- `DjangoModelPermissions`类：只有在用户经过身份验证并分配了相关模型权限时，才会获得授权访问相关模型。
- `DjangoModelPermissionsOrReadOnly`类：与前者类似，但可以给匿名用户访问API的可读权限。
- `DjangoObjectPermissions`类：只有在用户经过身份验证并分配了相关对象权限时，才会获得授权访问相关对象。通常与django-gaurdian联用实现对象级别的权限控制

### 自定义权限类

```python
# blog/permissions.py
from rest_framework import permissions

class IsOwnerOrReadOnly(permissions.BasePermission):
    """
    自定义权限只允许对象的创建者才能编辑它。"""
    def has_object_permission(self, request, view, obj):
        # 读取权限被允许用于任何请求，
        # 所以我们始终允许 GET，HEAD 或 OPTIONS 请求。
        if request.method in permissions.SAFE_METHODS:
            return True
        # 写入权限只允许给 article 的作者。
        return obj.author == request.user
# 然后修改我们的视图，IsOwnerOrReadOnly 的权限类，把它加入到ArticleDetail视图的permission_classes里  

```

### 更多权限设置的方式

 在前面的案例中，我们都是在基于类的API视图里通过**permission_classes**属性设置的权限类。如果你有些权限是全局或全站通用的，你还可以在settings.py中使用 `DEFAULT_PERMISSION_CLASSES` 全局设置默认权限策略 

```python
# settings
REST_FRAMEWORK = {
    'DEFAULT_PERMISSION_CLASSES': (
        'rest_framework.permissions.IsAuthenticated',
    )
}

# 如果没有指定,默认的就是AllowAny(允许无限制访问)
```

```python
# 给FBV视图添加权限
from rest_framework.decorators import api_view, permission_classes
from rest_framework.permissions import IsAuthenticated
from rest_framework.response import Response

@api_view(['GET'])
@permission_classes((IsAuthenticated, ))
def example_view(request, format=None):
    content = {
        'status': 'request was permitted'
    }
    return Response(content)
```

 **注意**：当通过类属性或装饰器设置新的权限类时，您会告诉视图忽略 settings.py 文件上设置的默认列表 

## 认证详解及如何使用Token认证

### 什么是认证 **(Authentication)** 

身份验证是将传入的请求对象(request)与一组标识凭据（例如请求来自的用户或其签名的令牌token）相关联的机制。REST framework 提供了一些开箱即用的身份验证方案，并且还允许你实现自定义方案。

DRF的每个认证方案实际上是一个类。你可以在视图中使用一个或多个认证方案类。REST framework 将尝试使用列表中的每个类进行身份验证，并使用成功完成验证的第一个类的返回的元组设置 `request.user` 和`request.auth`。

用户通过认证后request.user返回Django的User实例，否则返回`AnonymousUser`的实例。request.auth通常为None。如果使用token认证，request.auth可以包含认证过的token。

注：认证一般发生在权限校验之前。



###   REST framework自带哪些认证方案? 

- Session认证`SessionAuthentication`类：此认证方案使用Django的默认session后端进行身份验证。当客户端发送登录请求通过验证后，Django通过session将用户信息存储在服务器中保持用户的请求状态。Session身份验证适用于与你的网站在相同的Session环境中运行的AJAX客户端 (注：这也是Session认证的最大弊端)。
- 基本认证`BasicAuthentication`类：此认证方案使用HTTP 基本认证，针对用户的用户名和密码进行认证。使用这种方式后浏览器会跳出登录框让用户输入用户名和密码认证。基本认证通常只适用于测试。
- 远程认证`RemoteUserAuthentication`类：此认证方案为用户名不存在的用户自动创建用户实例。这个很少用，具体见文档。
- Token认证`TokenAuthentication`类：该认证方案是DRF提供的使用简单的基于Token的HTTP认证方案。当客户端发送登录请求时，服务器便会生成一个Token并将此Token返回给客户端，作为客户端进行请求的一个标识,以后客户端只需带上这个Token前来请求数据即可，无需再次带上用户名和密码。后面我们会详细介绍如何使用这种认证方案。

 注意：如果你在生产环境下使用BasicAuthentication和`TokenAuthentication`认证，你必须确保你的API仅在`https`可用。 

### 如何在DRF中使用指定的认证方案

#### 方式1:settings.py中设置默认的全局认证方案

```python
# settings
REST_FRAMEWORK = {
    'DEFAULT_AUTHENTICATION_CLASSES': (
        'rest_framework.authentication.BasicAuthentication',
        'rest_framework.authentication.SessionAuthentication',
    )}
```

#### 方式2:基于类的视图(CBV)中使用

```python
from rest_framework.authentication import SessionAuthentication, BasicAuthentication
from rest_framework.permissions import IsAuthenticated
from rest_framework.response import Response
from rest_framework.views import APIView

class ExampleView(APIView):
    authentication_classes = (SessionAuthentication, BasicAuthentication)
    permission_classes = (IsAuthenticated,)
```

#### 方式3:函数视图中使用

```python

@api_view(['GET'])
@authentication_classes((SessionAuthentication, BasicAuthentication))
@permission_classes((IsAuthenticated,))
def example_view(request, format=None):
    content = {
        'user': unicode(request.user),  # `django.contrib.auth.User` 实例。
        'auth': unicode(request.auth),  # None
    }
    return Response(content)
```

### 如何自定义认证方案

要实现自定义的认证方案，要继承`BaseAuthentication`类并且重写`.authenticate(self, request)`方法。如果认证成功，该方法应返回`(user, auth)`的二元元组，否则返回`None`。

在某些情况下，你可能不想返回`None`，而是希望从`.authenticate()`方法抛出`AuthenticationFailed`异常。

通常应该采取的方法是：

- 如果不尝试验证，返回`None`。还将检查任何其他正在使用的身份验证方案。
- 如果尝试验证但失败，则抛出`AuthenticationFailed`异常。无不进行其他任何认证方案和权限认证，立即返回错误响应。

也可以重写`.authenticate_header(self, request)`方法。如果实现该方法，则应返回一个字符串，该字符串将用作`HTTP 401 Unauthorized`响应中的`WWW-Authenticate`头的值。

如果`.authenticate_header()`方法未被重写，则认证方案将在未验证的请求被拒绝访问时返回`HTTP 403 Forbidden`响应。

```python
from django.contrib.auth.models import User
from rest_framework import authentication
from rest_framework import exceptions

class ExampleAuthentication(authentication.BaseAuthentication):
    def authenticate(self, request):
      #以自定义请求标头中名称为'X_USERNAME'提供的用户名作为用户对任何传入请求进行身份验证，其它类似自定义认证需求比如支持用户同时按用户名或email进行验证。
        username = request.META.get('X_USERNAME')
        if not username:
            return None

        try:
            user = User.objects.get(username=username)
        except User.DoesNotExist:
            raise exceptions.AuthenticationFailed('No such user')

        return (user, None)
```

###  **前后端分离时为何推荐token认证?** 

- Token无需存储降低服务器成本，session是将用户信息存储在服务器中的，当用户量增大时服务器的压力也会随着增大。
- 防御CSRF跨站伪造请求攻击，session是基于cookie进行用户识别的, cookie如果被截获，用户信息就容易泄露。
- 扩展性强，session需要存储无法共享，当搭建了多个服务器时其他服务器无法获取到session中的验证数据用户无法验证成功。而Token可以实现服务器间共享，这样不管哪里都可以访问到。
- Token可以减轻服务器的压力，减少频繁的查询数据库。
- 支持跨域访问, 适用于移动平台应用

###  如何使用TokenAuthentication？

```python
# 首先修改settings
INSTALLED_APPS = (
    ...
    'rest_framework.authtoken'
    )

# 其次,为用户生成令牌(token)
# 在创建用户时自动生成token,可以借助django的信号(signals)实现
@receiver(post_save, sender=settings.AUTH_USER_MODEL)
def create_auth_token(sender, instance=None, created=False, **kwargs):
    if created:
        Token.objects.create(user=instance)

# 如果已经创建了一些用户,可以使用shell为所有用户生成令牌
from django.contrib.auth.models import User
from rest_framework.authtoken.models import Token

for user in User.objects.all():    
    Token.objects.get_or_create(user=user)

# 在admin.py中给用户创建token
from rest_framework.authtoken.admin import TokenAdmin
TokenAdmin.raw_id_fields = ['user']

# 在3.6.4开始,可以使用以下命令新建或重置token
./manage.py drf_create_token <username> # 新建
./manage.py drf_create_token -r <username> # 重置

# 接下来,需要暴露用户获取token的url地址
from rest_framework.authtoken import views
urlpatterns += [
    url(r'^api-token-auth/', views.obtain_auth_token)
]
# 这样每当用户使用form表单或JSON将有效的username和password字段POST提交到以上视图时，obtain_auth_token视图将返回如下JSON响应：
{ 'token' : '9944b09199c62bcf9418ad846dd0e4bbdfc6ee4b' }

# 客户端拿到token后可以将其存储到本地cookie或localstorage里，下次发送请求时把token包含在Authorization HTTP头即可
Authorization: Token 9944b09199c62bcf9418ad846dd0e4bbdfc6ee4b

curl -X GET http://127.0.0.1:8000/api/example/ -H 'Authorization: Token 9944b09199c62bcf9418ad846dd0e4bbdfc6ee4b'
```

 默认的`obtain_auth_token`视图返回的json响应数据是非常简单的，只有token一项。如果你希望返回更多信息，比如用户id或email，就就要通过继承ObtainAuthToken类量身定制这个视图 

```python
from rest_framework.authtoken.views import ObtainAuthToken
from rest_framework.authtoken.models import Token
from rest_framework.response import Response

class CustomAuthToken(ObtainAuthToken):
    def post(self, request,*args,**kwargs):
        serializer =self.serializer_class(data=request.data,
                                           context={'request': request})
        serializer.is_valid(raise_exception=True)
        user = serializer.validated_data['user']
        token, created =Token.objects.get_or_create(user=user)
        returnResponse({
            'token': token.key,
            'user_id': user.pk,
            'email': user.email
        })

# 修改urls.py

urlpatterns +=[
    path('api-token-auth/',CustomAuthToken.as_view())
]
```

最后, DRF的TokenAuthentication类会从请求头中获取Token，验证其有效性。如果token有效，返回request.user。至此，整个token的签发和验证就完成了 

## 使用JWT认证(重要)

JSON Web Token是一种更新的使用token进行身份认证的标准。 。与DRF内置的TokenAuthentication方案不同，JWT身份验证不需要使用数据库来验证令牌, 而且可以轻松设置token失效期或刷新token, 是API开发中当前最流行的跨域认证解决方案 。Django中可以通过djangorestframework-simplejwt 这个第三方包轻松实现JWT认证 

###  什么是Json Web Token及其工作原理 

JSON Web Token（JWT）是一种开放标准，它定义了一种紧凑且自包含的方式，用于各方之间安全地将信息以JSON对象传输。由于此信息是经过数字签名的，因此可以被验证和信任。JWT用于为应用程序创建访问token，通常适用于API身份验证和服务器到服务器的授权。那么如何理解紧凑和自包含这两个词的含义呢?

- **紧凑**：就是说这个数据量比较少，可以通过url参数，http请求提交的数据以及http header多种方式来传递。
- **自包含**：这个字符串可以包含很多信息，比如用户id，用户名，订单号id等，如果其他人拿到该信息，就可以拿到关键业务信息。

 那么JWT认证是如何工作的呢? 首先客户端提交用户登录信息验证身份通过后，服务器生成一个用于证明用户身份的令牌(token)，也就是一个加密后的长字符串，并将其发送给客户端。在后续请求中，客户端以各种方式(比如通过url参数或者请求头)将这个令牌发送回服务器，服务器就知道请求来自哪个特定身份的用户了。 

JSON Web Token由三部分组成，这些部分由点（.）分隔，分别是header(头部)，payload(有效负载)和signature(签名)。

- **header(头部)**: 识别以何种算法来生成签名；
- **pyload(有效负载)**: 用来存放实际需要传递的数据；
- **signature(签名):** 安全验证token有效性，防止数据被篡改

 通过http传输的数据实际上是加密后的JWT，它是由两个点分割的base64-URL长字符串组成，解密后我们可以得到header, payload和signature三部分。我们可以简单的使用 [JWT官网](https://jwt.io/ )来生成或解析一个JWT 

###  **Django中如何使用JWT认证** 

`django-rest-framework-simplejwt`为Django REST框架提供了JSON Web令牌认证后端。它提供一组保守的默认功能来涵盖了JWT的最常见用例。它还非常容易扩展。 

```python
# 安装
pip install djangorestframework-simplejwt

# 配置setting.py,告诉DRF我们使用jwt认证作为后台认证方案
REST_FRAMEWORK = {
    'DEFAULT_FILTER_BACKENDS': [
        'django_filters.rest_framework.DjangoFilterBackend'
    ],
    'DEFAULT_AUTHENTICATION_CLASSES': [
        'rest_framework_simplejwt.authentication.JWTAuthentication',
    ],
}

# 提供用户可以获取和刷新token的urls地址，这两个urls分别对应TokenObtainPairView和TokenRefreshView两个视图
from django.contrib import admin
from django.urls import path, include
from reviews.views import ProductViewSet, ImageViewSet
from rest_framework.routers import DefaultRouter
from django.conf import settings
from django.conf.urls.static import static
from rest_framework_simplejwt.views import (
    TokenObtainPairView,
    TokenRefreshView,
)

router = DefaultRouter()
router.register(r'product', ProductViewSet, basename='Product')
router.register(r'image', ImageViewSet, basename='Image')

urlpatterns = [
    path('admin/', admin.site.urls),
    path('token/', TokenObtainPairView.as_view(), name='token_obtain_pair'),
    path('token/refresh/', TokenRefreshView.as_view(), name='token_refresh'),
    path('', include(router.urls)),
]

if settings.DEBUG:
    urlpatterns += static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
```

