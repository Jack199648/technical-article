# git使用文档

#### 简易的命令行入门教程:

Git 全局设置:

```
git config --global user.name "李杰"
git config --global user.email "1398207080@qq.com"
```

创建 git 仓库:

```
mkdir technical-article
cd technical-article
git init 
touch README.md
git add README.md
git commit -m "first commit"
git remote add origin https://gitee.com/Jack199648/technical-article.git
git push -u origin "master"
```

已有仓库?

```
cd existing_git_repo
git remote add origin https://gitee.com/Jack199648/technical-article.git
git push -u origin "master"
```



