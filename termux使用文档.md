# termux使用文档

## 安装code-server

```shell
pkg install nodejs-lts yarn # 安装nodejs最新长期支持版
npm install -g cnpm --registry=https://registry.npm.taobao.org # 配置国内镜像源
cnpm install -g npm # 升级npm版本
FORCE_NODE_VERSION=16 yarn global add code-server --ignore-engines  # 解决code-server只支持nodejs14的冲突
nano ~/.config/code-server/config.yaml # 修改code-server配置文件
nohup code-server &
```

## 安装开发环境

```shell
pkg install golang python
```

