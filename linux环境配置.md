# linux环境配置

```shell
#!/bin/bash

# 配置基础环境
apt update >/dev/null 2>&1 && apt upgrade -y >/dev/null 2>&1
# apt install wget -y
apt install net-tools wget -y >/dev/null 2>&1


# 一键修改root密码
## debian
echo root:111111|chpasswd
## centos
echo 111111|/usr/bin/passwd --stdin  root

# 配置frp
wget https://github.com/fatedier/frp/releases/download/v0.38.0/frp_0.38.0_linux_amd64.tar.gz
tar -zxvf frp_0.38.0_linux_amd64.tar.gz
cd frp_0.38.0_linux_amd64
vim frpc.ini
nohup ./frpc -c frpc.ini &

# 配置miniconda
wget https://repo.anaconda.com/miniconda/Miniconda3-latest-Linux-x86_64.sh && chmod +x Miniconda3-latest-Linux-x86_64.sh
./Miniconda3-latest-Linux-x86_64.sh

# 配置go
wget https://golang.google.cn/dl/go1.17.5.linux-amd64.tar.gz &&tar -C /usr/local -zxf go1.17.5.linux-amd64.tar.gz
echo "export PATH=$PATH:/usr/local/go/bin" >> ~/.bashrc
# 启用 Go Modules 功能
go env -w GO111MODULE=on
# 配置 GOPROXY 环境变量
# 1. 七牛 CDN
go env -w  GOPROXY=https://goproxy.cn,direct


# 配置蒲公英
wget "https://pgy.oray.com/dl/42/download?os=Ubuntu%20Linux(X86_64)" -O PgyVisitor_Ubuntu_2.3.0_x86_64.deb
dpkg -i PgyVisitor_Ubuntu_2.3.0_x86_64.deb
pgyvpn
46970489:001 111111
46970489:002 111111
46970489:003 111111

# 配置nodejs
wget https://nodejs.org/dist/v16.13.1/node-v16.13.1-linux-x64.tar.gz && tar -zxf node-v16.13.1-linux-x64.tar.gz -C /usr/local/
ln -s /usr/local/node-v16.13.1-linux-x64/bin/node /usr/local/bin/
ln -s /usr/local/node-v16.13.1-linux-x64/bin/npm /usr/local/bin/

# 配置pip源
pip install -i https://pypi.tuna.tsinghua.edu.cn/simple pip -U
pip config set global.index-url https://pypi.tuna.tsinghua.edu.cn/simple


# 配置conda
conda config --set show_channel_urls yes
nano .condarc

channels:
  - defaults
show_channel_urls: true
default_channels:
  - https://mirrors.tuna.tsinghua.edu.cn/anaconda/pkgs/main
  - https://mirrors.tuna.tsinghua.edu.cn/anaconda/pkgs/r
  - https://mirrors.tuna.tsinghua.edu.cn/anaconda/pkgs/msys2
custom_channels:
  conda-forge: https://mirrors.tuna.tsinghua.edu.cn/anaconda/cloud
  msys2: https://mirrors.tuna.tsinghua.edu.cn/anaconda/cloud
  bioconda: https://mirrors.tuna.tsinghua.edu.cn/anaconda/cloud
  menpo: https://mirrors.tuna.tsinghua.edu.cn/anaconda/cloud
  pytorch: https://mirrors.tuna.tsinghua.edu.cn/anaconda/cloud
  pytorch-lts: https://mirrors.tuna.tsinghua.edu.cn/anaconda/cloud
  simpleitk: https://mirrors.tuna.tsinghua.edu.cn/anaconda/cloud
conda clean -i
```

